��          �      <      �  &   �  8   �  
     7        T  5   g  3   �     �  0   �  (     >   B  2   �  >   �  :   �  6   .  D   e  6   �  �  �  &   �  C   �       <        V  =   q  ;   �     �  <        E  J   X  8   �  }   �     Z	     w	  !   �	     �	                       	                                               
                               Activity [Last {inactivity_threshold}] All characters have been added to {MEMBERAUDIT_APP_NAME} Compliance Maximum allowable inactivity, in <strong>days</strong>. Member Audit Asset Member Audit Secure Groups Integration v{__version__} Member Audit Skill Points [{skill_point_threshold}] Member Audit Skill Set Minimum allowable age, in <strong>days</strong>. Missing character:  Missing characters:  Not all of your characters are added to {MEMBERAUDIT_APP_NAME} The filter description that is shown to end users. User must possess <strong>one</strong> of the selected assets. {inactivity_threshold:d} day {inactivity_threshold:d} days {self.age_threshold:d} day {self.age_threshold:d} days {self.inactivity_threshold:d} day {self.inactivity_threshold:d} days {sp_threshold} skill point {sp_threshold} skill points Project-Id-Version: Korean (Alliance Auth Apps)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Korean <https://weblate.ppfeufer.de/projects/alliance-auth-apps/aa-member-audit-secure-groups-integration/ko/>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 5.0.2
 활동 [최근 {inactivity_threshold}] 모든 캐릭터가 {MEMBERAUDIT_APP_NAME}에 등록되었습니다 준수 사항 허용 최대 미접속 기간, <strong>일</strong> 단위. 아이템별 멤버 관리 멤버 관리 보안 그룹 인테그레이션 v{__version__} 스킬 포인트별 멤버 관리 [{skill_point_threshold}] 스킬 셋 별 멤버 관리 허용 최소 캐릭터 나이, <strong>일</strong> 단위. 빠진 캐릭터:  모든 캐릭터가 {MEMBERAUDIT_APP_NAME}에 추가되지 않았습니다 신청하는 사용자에게 보여지는 필터 설명. 사용자는 선택된 아이템 중 최소 <strong>한 개</strong> 이상의 아이템을 소유하고 있어야 합니다. {inactivity_threshold:d} 일 {self.age_threshold:d} 일 {self.inactivity_threshold:d} 일 {sp_threshold} 스킬 포인트 