��          �   %   �      0  &   1  &   X  8        �  
   �  7   �       5   .  3   d     �  0   �     �  (      >   )     h      �  2   �  >   �  X     :   l  6   �  D   �  6   #  �  Z  '     *   F  =   q  &   �  
   �  :   �     	  5   /	  2   e	     �	  3   �	  "   �	  +   
  C   2
     v
  #   �
  ;   �
  K   �
  i   ?  :   �  6   �  D     4   `             
                                          	                                                                        Active character:  Active characters:  Activity [Last {inactivity_threshold}] All characters have been added to {MEMBERAUDIT_APP_NAME} Character age [{age_threshold}] Compliance Maximum allowable inactivity, in <strong>days</strong>. Member Audit Asset Member Audit Secure Groups Integration v{__version__} Member Audit Skill Points [{skill_point_threshold}] Member Audit Skill Set Minimum allowable age, in <strong>days</strong>. Minimum allowable skill points. Missing character:  Missing characters:  Not all of your characters are added to {MEMBERAUDIT_APP_NAME} Please create a filter! Please create an audit function! The filter description that is shown to end users. User must possess <strong>one</strong> of the selected assets. Users must possess all of the skills in <strong>one</strong> of the selected skill sets. {inactivity_threshold:d} day {inactivity_threshold:d} days {self.age_threshold:d} day {self.age_threshold:d} days {self.inactivity_threshold:d} day {self.inactivity_threshold:d} days {sp_threshold} skill point {sp_threshold} skill points Project-Id-Version: German (Alliance Auth Apps)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: German <https://weblate.ppfeufer.de/projects/alliance-auth-apps/aa-member-audit-secure-groups-integration/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.0.2
 Aktiver Charakter:  Aktive Charaktere:  Aktivität [Letzte {inactivity_threshold}] Alle Charaktere wurden zu {MEMBERAUDIT_APP_NAME} hinzugefügt Alter des Charakters [{age_threshold}] Compliance Maximal zulässige Inaktivität in <strong>Tagen</strong>. Member Audit Asset Member Audit Secure Groups Integration v{__version__} Member Audit Skillpunkte [{skill_point_threshold}] Member Audit Skill Set Zulässiges Mindestalter in <strong>Tagen</strong>. Mindestens zulässige Skillpunkte. Fehlender Charakter:  Fehlende Charaktere:  Nicht alle Charaktere wurden zu {MEMBERAUDIT_APP_NAME} hinzugefügt Bitte erstelle einen Filter! Bitte erstelle eine Audit-Funktion! Die Filterbeschreibung welche den Benutzern angezeigt wird. Der Benutzer muss <strong>eines</strong> der ausgewählten Assets besitzen. Benutzer müssen über alle Fähigkeiten in <strong>einem</strong> der ausgewählten Skillsets verfügen. {inactivity_threshold:d} Tag {inactivity_threshold:d} Tage {self.age_threshold:d} Tag {self.age_threshold:d} Tage {self.inactivity_threshold:d} Tag {self.inactivity_threshold:d} Tage {sp_threshold} Skillpunkt {sp_threshold} Skillpunkte 